//silvio vera martinez 19339408-6
#include <stdio.h>
#include <stdlib.h>
#include <math.h>//incluimos la libreria math para poder usar pow posteriormente
#include "estudiantes.h"



float desvStd(estudiante curso[],float prom[], int promedio){
	//declaracion de variables y se crea el ciclo para poder obtener la desviacion estandar
	int i;
	float desviacion=0,cuadrados=0,diferencia=0,suma=0,sumatotal=0;
	for (int i = 0; i < 24; ++i){
		diferencia=prom[i] - promedio;
		cuadrados= pow(diferencia,2);//la funcion pow es una potencia (variable , cantidad de veces a elevebar)
		suma= suma +cuadrados;
		}
	sumatotal=suma/24;
	desviacion=pow(sumatotal,0.5);
	return desviacion;//retornamos la desviacion
}

float menor(float prom[]){//calculo para el numero menor con un simple ciclo for
	int i=0,n_menor=0;
	while (i<24){
		if (prom[i]>prom[i+1]){
			n_menor = prom[i+1];//asignamos los valores obtenidos a la varible
			return n_menor;
		}
		i=i+1;
	}
}


float mayor(float prom[]){//calculo para el numero mayor con un simple ciclo for
	int i=0,n_mayor=0;
	while (i<24){
		if (prom[i]>prom[i+1]){
			n_mayor = prom[i+1];//asignamos los valores obtenidos a la varible
			return n_mayor;
		}
		i=i+1;	
	}
	
}

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones 
	for (int i = 0; i < 24; ++i){//para el registro de notas se usa un ciclo for para recorrer la lista de alumnos obtenida del txt y pedir una a una las notas
		printf("\n Ingrese las notas para el estudiante: \n %s %s %s \n",curso[i].nombre, curso[i].apellidoP , curso[i].apellidoM);
		printf("\n Ingresar notas entre 1 y 7 ");
		printf("\n Ingresar notas de Controles");
		printf("\n Control 1:");scanf("%f",&curso[i].asig_1.cont1);
		printf("\n Control 2:");scanf("%f",&curso[i].asig_1.cont2);
		printf("\n Control 3:");scanf("%f",&curso[i].asig_1.cont3);
		printf("\n Control 4:");scanf("%f",&curso[i].asig_1.cont4);
		printf("\n Control 5:");scanf("%f",&curso[i].asig_1.cont5);
		printf("\n Control 6:");scanf("%f",&curso[i].asig_1.cont6);

		printf("\n Ingrese las notas de proyecto");
		
		printf("\n Proyecto 1:");scanf("%f",&curso[i].asig_1.proy1);
		printf("\n Proyecto 2:");scanf("%f",&curso[i].asig_1.proy2);
		printf("\n Proyecto 3:");scanf("%f",&curso[i].asig_1.proy3);
	
	int nota, prom;
	nota=(curso[i].asig_1.proy1 + curso[i].asig_1.proy2 + curso[i].asig_1.proy3 + curso[i].asig_1.cont1 + curso[i].asig_1.cont2 + curso[i].asig_1.cont3 + curso[i].asig_1.cont4 + curso[i].asig_1.cont5 + curso[i].asig_1.cont6);
	prom=nota/9;//usando todas las notas ingresadas se suman para luego sacar el promedio
	printf("\n el promedio es:%d",prom );

}
}
void clasificarEstudiantes(char path[], estudiante curso[]){
	//creacion de vatiables y punteros
	FILE *Aprobados;
	FILE *Reprobados;
	Aprobados=fopen("Aprobados.txt","w");//se abren los archivos para aprobados y reprobados en caso de que no esxitan el sistema los creara automaticamente
	Reprobados=fopen("Reprobados.txt","w");
	float promedio ; int prom;
	registroCurso(prom);//se designa el valor a la varible desde la funcion registro de curso
	int i=0;
	for (i=0;i <24;i++){//ciclos y condiciones para saber quien aprobo o reporbo
		if (prom[i]>4){
			fprintf(Aprobados, "%s %s %s",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
		}
		else{
		fprintf(Reprobados, "%s %s %s",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);
		}
	i=i+1;
	}
	fclose(Aprobados);//se cierran los archivos txt para evitar malgasto de memoria
	fclose(Reprobados);
}


void metricasEstudiantes(estudiante curso[]){	
	int i=0, desviacion,n_mayor, n_menor;//declaracion de variables
	float prom[i];

	menor(n_menor);
	mayor(n_mayor);
	desvStd(desviacion);

	printf("El promedio más bajo es de : %s %s %s",curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,"con promedio:%2.f",n_menor);
	printf("El promedio más alto es de : %s %s %s",curso[i].nombre, curso[i].apellidoP, curso[i].apellidoM,"con promedio:%2.f",n_mayor);
	printf("\n La desviacion estandar es: %.2d", desviacion);
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}